import React from "react";
import { StyleSheet, View, Text } from "react-native";

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content} />
        <Text> How to say it properly ?</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  content: {
    width: 300,
    height: 500,
    backgroundColor: "lightblue",
    borderRadius: 50
  }
});
